package co.simplon.alt3.exceptions;

public class InvalidBirthDateException extends Exception {
    public InvalidBirthDateException(String m) {
        super(m);
    }
    public InvalidBirthDateException() {
        super("La date de naissance ne peut pas être supérieure à la date actuelle !");
    }
}
