package co.simplon.alt3.exceptions;

public class NegativePriceException  extends Exception {
    public NegativePriceException(String m) {
        super(m);
    }
    public NegativePriceException() {
        super("Le prix ne peut pas être négatif !");
    }
}
