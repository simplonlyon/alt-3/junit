package co.simplon.alt3.exceptions;

public class TooManyDogsException extends Exception {
    public TooManyDogsException(String m) {
        super(m);
    }
}
