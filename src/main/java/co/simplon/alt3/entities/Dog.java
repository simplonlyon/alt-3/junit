package co.simplon.alt3.entities;

import java.time.LocalDate;

import co.simplon.alt3.exceptions.InvalidBirthDateException;
import co.simplon.alt3.exceptions.NegativePriceException;

public class Dog extends Organic {
    private float price;
    private Human owner;
    private Breed breed;
    /**
     * 
     */
    public Dog() {
    }
    /**
     * @param name
     * @param birthDate
     * @param price
     * @param owner
     * @throws InvalidBirthDateException
     * @throws NegativePriceException
     */
    public Dog(String name, LocalDate birthDate, float price, Human owner, Breed breed) throws InvalidBirthDateException, NegativePriceException {
        this.setName(name);
        this.setBirthDate(birthDate);
        this.setPrice(price);
        this.owner = owner;
        this.breed = breed;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }
    /**
     * @param price the price to set
     * @throws NegativePriceException
     */
    public void setPrice(float price) throws NegativePriceException {
        if (price < 0) {
            throw new NegativePriceException();
        }
        this.price = price;
    }
    /**
     * @return the owner
     */
    public Human getOwner() {
        return owner;
    }
    /**
     * @param owner the owner to set
     */
    public void setOwner(Human owner) {
        this.owner = owner;
    }
    
}
