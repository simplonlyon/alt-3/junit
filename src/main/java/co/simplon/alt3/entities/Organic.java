package co.simplon.alt3.entities;

import java.security.InvalidParameterException;
import java.time.LocalDate;

import co.simplon.alt3.exceptions.InvalidBirthDateException;
import co.simplon.alt3.utils.StringUtils;

public abstract class Organic {
    private String name;
    private LocalDate birthDate;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) 
        throws InvalidParameterException {
        if (name == null || name == "") {
            throw new InvalidParameterException("Le nom ne peut pas être vide");
        }
        this.name = StringUtils.titalizeString(name);
    }

        /**
     * @return the birthDate
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }
    /**
     * @param birthDate the birthDate to set
     * @throws InvalidBirthDateException
     */
    public void setBirthDate(LocalDate birthDate) throws InvalidBirthDateException {
        if (birthDate.isAfter(LocalDate.now())) {
            throw new InvalidBirthDateException("Vérifie !");
        }
        this.birthDate = birthDate;
    }
}
