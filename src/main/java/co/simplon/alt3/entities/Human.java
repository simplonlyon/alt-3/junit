package co.simplon.alt3.entities;

import java.time.LocalDate;
import java.util.ArrayList;

import co.simplon.alt3.exceptions.InvalidBirthDateException;
import co.simplon.alt3.exceptions.TooManyDogsException;

public class Human extends Organic {
    private static final int MAX_DOGS = 10000000;
    private String address;
    private ArrayList<Dog> dogs;

    public Human() {
        this.dogs = new ArrayList<>();
    }
    public Human(String name, LocalDate birthDate, String address) throws InvalidBirthDateException {
        this.setName(name);
        this.setBirthDate(birthDate);
        this.address = address;
        this.dogs = new ArrayList<>();
    }

    public int getMaxDogs() {
        return Human.MAX_DOGS;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }
    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    public Dog getDog(int index) {
        return this.dogs.get(index);
    }
    public void addDog(Dog d) throws TooManyDogsException {
        if (this.getNumberOfDogs() >= this.getMaxDogs()) {
            throw new TooManyDogsException("Trop de chiens ! Vous en avez déjà " + this.getNumberOfDogs());
        }
        this.dogs.add(d);
    }
    public void addAllDogs(ArrayList<Dog> dogs) {
        this.dogs.addAll(dogs);
    }
    public int getNumberOfDogs() {
        return this.dogs.size();
    }
}
