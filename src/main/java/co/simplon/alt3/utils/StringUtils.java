package co.simplon.alt3.utils;

public class StringUtils {
    public static String titalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
          if (!found && Character.isLetter(chars[i])) {
            chars[i] = Character.toUpperCase(chars[i]);
            found = true;
          } else if (!Character.isLetter(chars[i])) {
            found = false;
          }
        }
        return String.valueOf(chars);
      }
}
