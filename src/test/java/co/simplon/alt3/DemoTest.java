package co.simplon.alt3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
public class DemoTest {

    @Test
    @Order(2)
    void testAdditionner_Smoke() {
        Demo d = new Demo();
        assertEquals(
            d.additionner(1, 1),
            2
        );
    }

    @Test
    @Order(1)
    void testAdditionner_SmokeTrue() {
        Demo d = new Demo();
        assertTrue(d.additionner(1, 1) == 2);
    }

    @Test
    void testDiviser_Smoke() {
        Demo d = new Demo();
        assertEquals(d.diviser(1, 1), 1);
    }

    @Test
    void testDiviser_DivideByZero() {
        Demo d = new Demo();
        assertThrows(
            ArithmeticException.class,
            () -> d.diviser(1, 0)
        );
    }

}
