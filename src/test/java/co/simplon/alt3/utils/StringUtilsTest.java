package co.simplon.alt3.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class StringUtilsTest {
    @Test
    void testTitalizeString_TitalizeOneWord() {
        assertEquals("Jean", StringUtils.titalizeString("jean"));
    }
    @Test
    void testTitalizeString_TitalizeOneWordNormalized() {
        assertEquals("Jean", StringUtils.titalizeString("jEaN"));
    }
    @Test
    void testTitalizeString_TitalizeTwoWordsWithSpaceNormalized() {
        assertEquals("Jean Demel", StringUtils.titalizeString("jean demel"));
    }
    @Test
    void testTitalizeString_TitalizeTwoWordsWithDashNormalized() {
        assertEquals("Pierre-Yves", StringUtils.titalizeString("pierre-yVEs"));
    }
    @Test
    void testTitalizeString_TitalizeTwoWordsWithSpecialCharsNormalized() {
        assertEquals("Jean D'Ormesson", StringUtils.titalizeString("jean d'oRmesson"));
    }
}
