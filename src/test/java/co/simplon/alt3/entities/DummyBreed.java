package co.simplon.alt3.entities;

import java.util.ArrayList;

public class DummyBreed extends Breed {
    public DummyBreed() {
        super(1, "", new ArrayList<String>(), new ArrayList<String>());
    }
}
