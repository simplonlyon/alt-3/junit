package co.simplon.alt3.entities;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.InvalidParameterException;

import org.junit.jupiter.api.Test;

public class OrganicTest {
    @Test
    void testSetName_EmptyName() {
        ConcreteOrganic o = new ConcreteOrganic();
        assertThrows (
            InvalidParameterException.class,
            () -> o.setName("")
        );
    }

    @Test
    void testSetName_NullName() {
        ConcreteOrganic o = new ConcreteOrganic();
        assertThrows (
            InvalidParameterException.class,
            () -> o.setName(null)
        );
    }
}
