package co.simplon.alt3.entities;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.InvalidParameterException;

import org.junit.jupiter.api.Test;

import co.simplon.alt3.exceptions.TooManyDogsException;

public class HumanTest {

    @Test
    void testAddDog_TooManyDogs() throws TooManyDogsException {
        Human h = new FakeHuman();
        assertThrows(
            TooManyDogsException.class, 
            () -> h.addDog(new Dog())
        );
    }
}
