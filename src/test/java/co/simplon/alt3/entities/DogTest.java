package co.simplon.alt3.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import co.simplon.alt3.exceptions.InvalidBirthDateException;
import co.simplon.alt3.exceptions.NegativePriceException;

public class DogTest {
    @Test
    void testSetBirthDate_Smoke() throws InvalidBirthDateException {
        Dog d = new Dog("Gérard", LocalDate.of(2020, 1, 8), 1000, new Human(), new DummyBreed());
        assertEquals(d.getName(), "Gérard");
    }

    @Test
    void testSetBirthDate_BirthDateInFuture() {
        Dog d = new Dog();
        assertThrows(
            InvalidBirthDateException.class,
            () -> d.setBirthDate(LocalDate.of(2224, 1, 8))
        );
    }

    @Test
    void testSetPrice_UnderZero() {
        Dog d = new Dog();
        assertThrows(
            NegativePriceException.class,
            () -> d.setPrice(-45)
        );
    }
}
