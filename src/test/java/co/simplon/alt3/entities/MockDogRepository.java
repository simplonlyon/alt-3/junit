package co.simplon.alt3.entities;

import java.util.List;

import co.simplon.alt3.repositories.DogRepository;

public class MockDogRepository extends DogRepository {
    @Override
    public List<Dog> getAll() {
        return List.of(
            new Dog(),
            new Dog(),
            new Dog()
        );
    }
}
